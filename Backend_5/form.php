<!DOCTYPE html>
<html lang="ru">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form</title>
    <link rel="stylesheet" href="main.css">
</head>

<body>
    <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>'); } ?>
    <b><a href="login.php">Login</a></b>
    <div id="form" class="col-12 order-first order-md-2 px-0 py-4">
        <h3>Форма</h3>
        <form action="" method="POST">
            <label>Ваше имя - <br>
                <input name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" type="text" placeholder="Имя :" />
            </label><br />
            <label>
                Ваш email - <br />
                <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" placeholder="test@example.com" type="email" />
            </label><br />
            <label>
                Год рождения:<br />
                <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>">
                    <?php for($i = 1900; $i < 2021; $i++) {?>
                    <option value="<?php print $i; ?>"><?= $i; ?></option>
                    <?php }?>
                </select>
            </label><br />
            <label>
                Пол - <br />
                <input type="radio" name="sex" <?php if($values['sex']==0){print 'checked';} ?> value="0" />
                М
                <input type="radio" name="sex" <?php if($values['sex']==1){print 'checked';} ?> value="1" />
                Ж
            </label><br />
            Количество конечностей -<br />
            <input type="radio" checked="checked" name="limb" value="1" <?php if($values['limb'] == 1){print 'checked';}?> />
            1
            <input type="radio" name="limb" value="2" <?php if($values['limb'] == 2){print 'checked';}?> />
            2
            <br />
            <input type="radio" name="limb" value="3" <?php if($values['limb'] == 3){print 'checked';}?> />
            3
            <label><input type="radio" <?php if ($values['limb']==4){print 'checked';} ?> name="limb" value="4" />

                4
            </label><br />
            <label>
                Биография - <br />
                <textarea name="bio" placeholder="Biography : " <?php print $values['bio']; ?>></textarea>
            </label><br />
            <label>
                Сверхспособности -
                <br />
                <select name="power[]" <?php if ($errors['power']) {print 'class="error"';} ?> multiple="multiple">
                    <option value="god">Бессмертие</option>
                    <option value="clip">Прохождение сквозь стены</option>
                    <option value="fly">Левитация</option>
                </select>
            </label><br />
            <label>
                <input type="checkbox" name="check-1" />
                С контрактом ознакомлен
                <br />
                <input type="submit" value="Отправить" />
            </label>
        </form>
    </div>
</body>

</html>
